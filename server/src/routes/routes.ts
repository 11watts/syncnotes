import { Request, Response } from "express";
import { AuthController } from "../controllers/authController";
import { NoteController } from "../controllers/noteController";

export class Routes {
    public noteController: NoteController = new NoteController();
    public authController: AuthController = new AuthController();

    public routes(app: any): void {
        app.route("/note")
            .get(this.noteController.getNotes);

        app.route("/note/:noteTitle")
            .get(this.noteController.getNoteByTitle)
            .put(this.noteController.updateNote)
            .delete(this.noteController.deleteNote);

        app.route("/auth")
            .post(this.authController.auth);
    }
}
