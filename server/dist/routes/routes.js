"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const authController_1 = require("../controllers/authController");
const noteController_1 = require("../controllers/noteController");
class Routes {
    constructor() {
        this.noteController = new noteController_1.NoteController();
        this.authController = new authController_1.AuthController();
    }
    routes(app) {
        app.route("/note")
            .get(this.noteController.getNotes);
        app.route("/note/:noteTitle")
            .get(this.noteController.getNoteByTitle)
            .put(this.noteController.updateNote)
            .delete(this.noteController.deleteNote);
        app.route("/auth")
            .post(this.authController.auth);
    }
}
exports.Routes = Routes;
//# sourceMappingURL=routes.js.map