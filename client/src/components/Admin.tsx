import * as React from 'react';
import { deleteNote, getAllNotes, login } from '../utils/api';

interface INote {
    _id: string;
    delete: (id: string) => void;
    title: string;
}

interface IState {
    notes: INote[],
    password: string,
    auth: boolean,
    error: string,
}

const Note: React.SFC<INote> = (props) => {
    return (
        <div style={styles.row}>
            <div style={styles.cell}>
                <a target="_blank" rel="noopener noreferrer" href={props.title}>{props.title}</a>
            </div>
            <div style={styles.cell}>
                <button className="deleteBtn" onClick={() => {
                    props.delete(props.title);
                }}> Delete <i className="fas fa-trash" /></button>
            </div>
        </div>
    );
}

class Admin extends React.Component<{}, IState> {

    constructor(props: any) {
        super(props);
        this.state = { notes: [{ _id: '12', title: 'title 12', delete: this.delete }], password: '', auth: false, error: '' };
        this.login = this.login.bind(this);
        this.delete = this.delete.bind(this);
    }

    public componentDidMount() {
        getAllNotes().then((res) => {
            this.setState({ notes: res.data });
        });
    }

    public render() {
        const { notes, password, auth, error } = this.state;
        return (
            <div style={styles.container}>
                {
                    auth ? (
                        <div style={styles.table}>
                            <div style={styles.row}>
                                <div style={styles.cell}><p>Note</p></div>
                                <div style={styles.cell}><p>Delete</p></div>
                            </div>
                            {notes.map((note) => (
                                <Note key={note._id} _id={note._id} delete={this.delete} title={note.title} />
                            ))}
                        </div>
                    ) : (
                            <div style={styles.table}>
                                <div style={styles.row}>
                                    <div style={{...styles.cell, flexDirection: 'row'}}>
                                        <p>
                                            To view and delete notes please login.
                                        </p>
                                        <p style={{color: 'red'}}>{error}</p>
                                    </div>
                                </div>
                                <div style={styles.row}>
                                    <div style={{ ...styles.cell, flex: 4 }}>
                                        <label htmlFor="admin-password" style={{ paddingRight: 8 }}>Admin Password:</label>
                                        <input id="admin-password" type="password" placeholder="password"
                                            onChange={(e: React.FormEvent<HTMLInputElement>) => {
                                                this.setState({ password: e.currentTarget.value });
                                            }}
                                            onKeyPress={(target) => {
                                                if (target.key === "Enter") {
                                                    this.login();
                                                }
                                            }}
                                            value={password} />
                                    </div>
                                    <div style={{ ...styles.cell, flex: 1 }}>
                                        <button className="loginBtn" onClick={this.login}> Login </button>
                                    </div>
                                </div>
                            </div>
                        )
                }
            </div>
        );
    }

    public login() {
        login(this.state.password).then((res) => {
            const auth = res.data.auth;
            const error = auth ? '' : 'Login failed!'
            this.setState({ auth, error });
        });
    }

    public delete(title: string) {
        deleteNote(title, this.state.password).then((res) => {
            const notes = this.state.notes.filter((note) => note.title !== title);
            this.setState({ notes });
        });
    }
}

export default Admin;

const styles = {
    cell: {
        alignItems: 'center',
        borderBottom: '1px solid black',
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        padding: 8,
    },
    container: {
        flex: 1,
        minWidth: 400,
    },
    row: {
        display: 'flex',
        flex: 1,
        minHeight: 50,
    },
    table: {
        display: 'flex',
        flex: 1,
        flexDirection: 'column' as 'column',
    },
}
