import * as React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Admin from './components/Admin';
import Notepad from './components/Notepad';
import Redirect from './components/Redirect';

interface IState {
    theme: {
        name: string;
        textareaColor: string;
        titleColor: string;
        headerColor: string;
        fontColor: string;
        backgroundColor: string;
    };
}

class App extends React.Component<{}, IState> {
    constructor(props: any) {
        super(props);
        this.state = {
            theme: themes.Light,
        };
        this.changeTheme = this.changeTheme.bind(this);
    }

    public changeTheme(event: React.ChangeEvent<HTMLSelectElement>) {
        this.setState({ theme: themes[event.target.value] });
    }

    public render() {
        const { backgroundColor } = this.state.theme;

        return (
            <BrowserRouter>
                    <>
                        <div className="container" style={{...styles.container, backgroundColor}}>
                        <Route exact={true} path="/" component={Redirect} />
                            <Switch>
                                <Route exact={true} path="/admin" render={() => {
                                    return <Admin />;
                                }} />
                                <Route path="/*" render={props => {
                                    return (
                                        <Notepad 
                                            title={props.match.params[0]}
                                            changeTheme={this.changeTheme}
                                            theme={this.state.theme} />
                                    );
                                }} />
                            </Switch>
                        </div>
                    </>
            </BrowserRouter>
        );
    }
}

export default App;

const styles = {
    container: {
        alignItems: 'center',
        backgroundColor: '#BBB5AC',
        display: 'flex',
        flexDirection: 'column' as 'column',
        margin: '0px',
        minHeight: '100%',
        padding: '0px',
    }
};

const themes = {
    Dark: {
        backgroundColor: '#93A1A1',
        fontColor: '#93A1A1',
        headerColor: '#002B36',
        name: 'Dark',
        textareaColor: '#073642',
        titleColor: '#93A1A1',
    },
    Light: {
        backgroundColor: '#E0E2E4',
        fontColor: '#073642',
        headerColor: '#268BD2',
        name: 'Light',
        textareaColor: '#FEFEFE',
        titleColor: '#073642',
    },
    Paper: {
        backgroundColor: '#36311F',
        fontColor: '#085F51',
        headerColor: '#ADA296',
        name: 'Paper',
        textareaColor: '#E3DCC2',
        titleColor: '#000',
    },
};