import app from "./app";
import logger from "./logger";

const PORT = process.env.PORT;

app.listen(PORT, () => {
    logger.info(`Express server listening on port ${PORT}`);
});
