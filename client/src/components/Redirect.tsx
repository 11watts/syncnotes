import * as React from 'react';


class Redirect extends React.Component<{}, {}> {
    constructor(props: any) {
        super(props);
        props.history.push(Math.random().toString(36).substring(4));
    }

    public render() {
        return <p>Redirecting...</p>;
    }
}

export default Redirect;
