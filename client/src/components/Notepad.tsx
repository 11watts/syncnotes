import * as React from 'react';
import { getByTitle, updateNote } from '../utils/api';
import Header from './Header';

interface IProps {
    changeTheme: (event: React.ChangeEvent<HTMLSelectElement>) => void;
    theme: {
        name: string;
        textareaColor: string;
        titleColor: string;
        headerColor: string;
        fontColor: string;
        backgroundColor: string;
    };
    title: string;
}

interface IState {
    note: string;
    loading: boolean;
    saved: boolean;
}

class Notepad extends React.Component<IProps, IState> {
    private timeout: NodeJS.Timeout;

    constructor(props: any) {
        super(props);
        this.state = { note: '', loading: true, saved: true };
        this.changeText = this.changeText.bind(this);
        this.saveNote = this.saveNote.bind(this);
    }

    public componentDidMount() {
        getByTitle(this.props.title).then((res) => {
            const note = res.data === null ? '' : res.data.text;
            this.setState({ note, loading: false });
        });
    }

    public render() {
        const { note, loading, saved } = this.state;
        const { title, changeTheme, theme  } = this.props;
        const backgroundColor = theme.textareaColor;
        const color = theme.fontColor;

        return (
            <>
                <Header changeTheme={changeTheme} theme={theme} title={title} saved={saved} />
                <div className="note-container">
                    <textarea
                        ref={(input) => {
                            if (input != null) {
                                input.focus();
                            }
                        }}
                        value={note}
                        spellCheck={false}
                        onChange={this.changeText}
                        disabled={loading}
                        style={{...styles.notepad, backgroundColor, color}} />
                </div>
            </>
        );
    }

    private changeText(e: React.ChangeEvent<HTMLTextAreaElement>) {
        this.setState({ note: e.target.value, saved: false });
        clearTimeout(this.timeout);
        this.timeout = setTimeout(() => {
            this.saveNote();
        }, 3000);
    }

    private saveNote() {
        updateNote(this.props.title, this.state.note).then((response) => {
            console.log(response);
            this.setState({ saved: true });
        });
    }
}

export default Notepad;

const styles = {
    notepad: {
        boxSizing: 'border-box' as 'border-box',
        minHeight: '100%',
        padding: '12px',
        resize: 'none' as 'none',
        width: '100%',
    }
}