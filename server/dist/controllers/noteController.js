"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const crypto_js_1 = require("crypto-js");
const mongoose = __importStar(require("mongoose"));
const logger_1 = __importDefault(require("../logger"));
const noteModel_1 = require("../models/noteModel");
const Note = mongoose.model("Note", noteModel_1.NoteSchema);
class NoteController {
    getNotes(req, res) {
        Note.find({}, (err, note) => {
            if (err) {
                res.json(err);
            }
            res.json(note);
        });
    }
    getNoteByTitle(req, res) {
        const title = req.params.noteTitle;
        logger_1.default.info(`Looking up ${title}...`);
        Note.findOne({ title })
            .then((doc) => {
            res.json(doc);
        })
            .catch((err) => {
            logger_1.default.error(err);
            res.json({ error: true });
        });
    }
    updateNote(req, res) {
        const { title, text } = req.body;
        Note.findOneAndUpdate({ title }, { title, text }, { upsert: true })
            .then((doc) => {
            logger_1.default.info(doc);
        })
            .catch((err) => {
            logger_1.default.error(err);
        });
        res.json();
    }
    deleteNote(req, res) {
        const { _id, password } = req.body;
        const title = req.params.noteTitle;
        const hash = crypto_js_1.SHA256(password).toString();
        logger_1.default.info(title);
        logger_1.default.info(hash);
        logger_1.default.info(process.env.PASSWORD_HASH);
        if (hash === process.env.PASSWORD_HASH) {
            logger_1.default.info("password match");
            Note.deleteOne({ title })
                .then((doc) => {
                logger_1.default.info(doc);
            })
                .catch((err) => {
                logger_1.default.error(err);
            });
        }
        else {
            logger_1.default.error("password failed");
        }
        res.json({ _id, hash, title });
    }
}
exports.NoteController = NoteController;
//# sourceMappingURL=noteController.js.map