import * as bodyParser from "body-parser";
import cors from "cors";
import * as dotenv from "dotenv";
import express from "express";
import mongoose from "mongoose";
import logger from "./logger";
import { Routes } from "./routes/routes";

class App {

    public app: express.Application;
    public routePrv: Routes = new Routes();

    constructor() {
        dotenv.config();
        this.app = express();
        this.config();
        this.routePrv.routes(this.app);
        this.mongoSetup();
    }

    private config(): void {
        const options: cors.CorsOptions = {
            allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
            credentials: true,
            methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
            origin: "*",
            preflightContinue: false
          };

        this.app.use(cors(options));
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
    }

    private mongoSetup(): void {
        const MONGO_URL: string = process.env.MONGO_URL;
        (mongoose as any).Promise = global.Promise;
        mongoose.connect(MONGO_URL, {
            useCreateIndex: true,
            useNewUrlParser: true,
        });
    }

}

export default new App().app;
