"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.NoteSchema = new mongoose_1.Schema({
    created_date: {
        default: Date.now,
        type: Date,
        unique: true,
    },
    text: {
        required: "Enter text",
        type: String,
    },
    title: {
        required: "Enter a title",
        type: String,
    },
});
//# sourceMappingURL=noteModel.js.map