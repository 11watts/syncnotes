import * as React from 'react';

interface IProps {
    changeTheme: (event: React.ChangeEvent<HTMLSelectElement>) => void;
    title: string;
    theme: {
        name: string;
        textareaColor: string;
        titleColor: string;
        headerColor: string;
        fontColor: string;
        backgroundColor: string;
    };
    saved: boolean;
}

class Header extends React.Component<IProps, {}> {
    constructor(props: any) {
        super(props);
    }

    public render() {
        const { changeTheme, title, saved, theme } = this.props;
        const backgroundColor = theme.headerColor;
        const color = theme.titleColor;
        const name = theme.name;

        return (
            <div className="header" style={{...styles.header, backgroundColor, color}}>
                {saved ?
                    (
                        <div style={styles.info} className="btn default">
                            Saved <i className="fas fa-check" />
                        </div>
                    ) :
                    (
                        <div style={styles.info} className="btn default">
                            Saving <i className="fas fa-spinner fa-spin" />
                        </div>
                    )}
                <div style={styles.title}>{title}</div>
                <div style={styles.info}>
                    <label>
                        <select value={name} onChange={changeTheme} style={{background: backgroundColor, color}}>
                            <option value="Light">Light</option>
                            <option value="Dark">Dark</option>
                            <option value="Paper">Paper</option>
                        </select>
                    </label>
                </div>
            </div >
        );
    }
}

export default Header;

const styles = {
    header: {
        display: 'flex',
        height: '3em',
        justifyContent: 'space-between',
    },
    info: {
        alignSelf: 'center',
        margin: '8px',
    },
    title: {
        fontSize: '1.8em',
    },
}