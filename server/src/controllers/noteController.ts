import { SHA256 } from "crypto-js";
import { Request, Response } from "express";
import * as mongoose from "mongoose";
import logger from "../logger";
import { NoteSchema } from "../models/noteModel";

const Note = mongoose.model("Note", NoteSchema);

export class NoteController {

    public getNotes(req: Request, res: Response) {
        Note.find({}, (err, note) => {
            if (err) {
                res.json(err);
            }
            res.json(note);
        });
    }

    public getNoteByTitle(req: Request, res: Response) {
        const title = req.params.noteTitle;
        logger.info(`Looking up ${title}...`);
        Note.findOne({ title })
            .then((doc) => {
                res.json(doc);
            })
            .catch((err) => {
                logger.error(err);
                res.json({ error: true });
            });
    }

    public updateNote(req: Request, res: Response) {
        const { title, text } = req.body;
        Note.findOneAndUpdate({ title }, { title, text }, { upsert: true })
            .then((doc) => {
                logger.info(doc);
            })
            .catch((err) => {
                logger.error(err);
            });
        res.json();
    }

    public deleteNote(req: Request, res: Response) {
        const { _id, password } = req.body;
        const title = req.params.noteTitle;
        const hash = SHA256(password).toString();

        logger.info(title);
        logger.info(hash);
        logger.info(process.env.PASSWORD_HASH);

        if (hash === process.env.PASSWORD_HASH) {
            logger.info("password match");
            Note.deleteOne({ title })
            .then((doc) => {
                logger.info(doc);
            })
            .catch((err) => {
                logger.error(err);
            });
        } else {
            logger.error("password failed");
        }

        res.json({ _id, hash, title });
    }

}
