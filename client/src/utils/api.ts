import axios from "axios";
import * as config from "../config.json";

const URL: string = config.apiUrl;

export const getByTitle = (title: string) => {
    return axios.get(`${URL}note/${title}`);
}

export const updateNote = (title: string, text: string) => {
    return axios.put(`${URL}note/${title}`, { title, text });
}

export const getAllNotes = () => {
    return axios.get(`${URL}note/`);
}

export const deleteNote = (title: string, password: string) => {
    return axios.delete(`${URL}note/${title}`, { data: { password } });
}

export const login = (password: string) => {
    return axios.post(`${URL}auth/`, { password });
}