#Syncnotes

Syncnotes is a shrib like online notepad created with typescript, react, & express.js. You can try it out at http://syncnotes.frankgarza.net.

![demo image](https://bitbucket.org/11watts/syncnotes/raw/eb49b0a5d422a2ab7445f5ee56cb17e95623e368/imgs/demo.gif)