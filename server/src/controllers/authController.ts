import { SHA256 } from "crypto-js";
import { Request, Response } from "express";

export class AuthController {
    public auth(req: Request, res: Response) {
        const { password } = req.body;
        const hash = SHA256(password).toString();

        const auth = (hash === process.env.PASSWORD_HASH);

        res.json({ auth });
    }
}
