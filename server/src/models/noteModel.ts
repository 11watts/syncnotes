import { Schema } from "mongoose";

export const NoteSchema = new Schema({
    created_date: {
        default: Date.now,
        type: Date,
        unique: true,
    },
    text: {
        required: "Enter text",
        type: String,
    },
    title: {
        required: "Enter a title",
        type: String,
    },
});
